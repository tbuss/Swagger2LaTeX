# Swagger2LaTeX
A script to turn a [Swagger](https://swagger.io/) YAML file into LaTeX syntax for documentatioal purposes

# Usage
 1) Install [Groovy](http://groovy-lang.org/), the objectively best programming language for the JVM!
 
 2) Clone the repository
 
 3) Inside the repository, run `./Swagger2LaTeX.groovy SWAGGER-FILE > specification.tex`
 
 4) ?
 
 5) Profit
