#!/usr/bin/env groovy
/*
 * Created by Thomas Buß on 09.07.2017
 */

@Grab('org.yaml:snakeyaml:1.17')

import org.yaml.snakeyaml.Yaml

@Singleton
class Config {
    def static final firstColumnWidthRelative = "0.25"
    def static final language = "de"
}

def static tr(String lang, String key) {
    def languages = [
            en:[
                    'number'           : 'number',
                    'api_specification': 'API specification',
                    'summary'          : 'summary',
                    'parameters'       : 'parameters',
                    'array'            : 'array',
                    'required'         : 'required',
                    'given_in'         : 'given in'
            ],
            de:[
                    'number'           : 'Zahl',
                    'api_specification': 'API Spezifikation',
                    'summary'          : 'Zusammenfassung',
                    'parameters'       : 'Parameter',
                    'given_in'         : 'Angegeben in'
            ]
    ]
    return languages[lang][key]?:languages['en'][key]
}

def static tr(String key) { tr(Config.language, key) }

def static formatParameter(parameter, boolean first) {
    """
   \\begin{tabularx}{\\textwidth}{p{$Config.firstColumnWidthRelative\\textwidth}X}
        ${if (!first) "\\hline" else ""}
        $parameter.name${parameter.required ? "*" : ""}: ${formatType(parameter)} & $parameter.description \\\\ 
        &     ${tr('given_in')}: $parameter.in
   \\end{tabularx}"""
}

def static formatSchema(String schema) {
    return schema.substring(schema.lastIndexOf('/') + 1, schema.length())
}

def static formatType(parameter) {
    if (parameter.type == "number")
        return parameter.format
    if (parameter.type == "array")
        return parameter.items.type + "[]"
    if (parameter.schema)
        return formatSchema(parameter.schema['$ref'] as String)
    return parameter.type
}

if (!args || !args[0]) System.exit(1)
def inFile = args[0] as File
def swagger = new Yaml().load(inFile.text)

println "\\chapter{${tr('api_specification')}}"
println swagger.info.description
swagger.paths.each { pathKey, path ->
    path.each { verbKey, verb ->
        println ""
        println "\\section{$pathKey (\\texttt{${verbKey.toUpperCase()}})}"
        println "   \\subsection*{${tr('summary')}}"
        println "   $verb.summary"
        println ""
        if (verb.parameters) {
            println "   \\subsection*{${tr('parameters')}}"
            verb.parameters.eachWithIndex { parameter, index ->
                println formatParameter(parameter, index == 0)
            }
        }
    }
}
